using UnrealBuildTool;

public class FuzziesTarget : TargetRules
{
	public FuzziesTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("Fuzzies");
	}
}
